## TripleO Minor Updates!

## Making Backups:
https://docs.openstack.org/project-deploy-guide/tripleo-docs/latest/post_deployment/backup_and_restore/00_index.html

#### Undercloud:
~~~
pass=$(sudo /bin/hiera -c /etc/puppet/hiera.yaml mysql::server::root_password)
sudo podman exec $(sudo podman ps --filter name=mysql -q) bash -c "mysqldump -uroot -p${pass} --opt --all-databases" > undercloud-all-databases.sql
~~~

#### Overcloud:
~~~
podman exec -it $(podman ps --filter name=galera -q) bash -c "mysqldump -u root --single-transaction --all-databases" > overcloud_db_$(date +%F).sql

podman exec -it $(podman ps --filter name=galera -q) bash

mysql -uroot -s -N -e "SELECT CONCAT('\"SHOW GRANTS FOR ''',user,'''@''',host,''';\"') FROM mysql.user where (length(user) > 0 and user NOT LIKE 'root')" | xargs -n1 mysql -u root -s -N -e | sed 's/$/;/' > /var/tmp/grants_$(date +%F).sql

exit

podman cp $(podman ps --filter name=galera -q):/var/tmp/grants_$(date +%F).sql .
~~~

## Undercloud Update:
https://docs.openstack.org/project-deploy-guide/tripleo-docs/latest/post_deployment/upgrade/undercloud.html

Handy dnf command to install the latest tripleo-repos RPM:
~~~
sudo dnf install https://trunk.rdoproject.org/centos8/component/tripleo/current/$(xmllint --html <(curl https://trunk.rdoproject.org/centos8/component/tripleo/current/) | egrep 'a href="python3-tripleo-repos' | cut -f 3 -d '>' | cut -f 1 -d '<')
~~~

#### Overcloud Update:
https://docs.openstack.org/project-deploy-guide/tripleo-docs/latest/post_deployment/upgrade/minor_update.html

#### Update Prepare environment file:
https://github.com/openstack/tripleo-heat-templates/blob/master/environments/lifecycle/update-prepare.yaml

#### Which gets appended here:
https://github.com/openstack/python-tripleoclient/blob/master/tripleoclient/v1/overcloud_update.py#L80-L82

#### Update Converge environment file:
https://github.com/openstack/tripleo-heat-templates/blob/master/environments/lifecycle/update-converge.yaml

#### Appended here:
https://github.com/openstack/python-tripleoclient/blob/master/tripleoclient/v1/overcloud_update.py#L264-L266

#### Developer Documentation for the update process:
https://docs.openstack.org/tripleo-docs/latest/upgrade/developer/upgrades/minor_update.html
