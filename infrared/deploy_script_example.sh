# Set the HOST and KEY variables to point to your Hypervisor and local SSH Key
# The SSH key should have already been copied to the Hypervisor using
# ssh-copy-id root@$HOST
#
# Usage: 
# bash deploy_script_example.sh provision
# bash deploy_script_example.sh undercloud
# bash deploy_script_example.sh overcloud
#
# Cleanup existing deployments using:
# bash deploy_script_example.sh clean


provision(){
   infrared virsh --host-address $HOST \
   --host-key ~/.ssh/id_ecdsa \
   --topology-nodes "undercloud:1,controller:3,compute:2" \
   --image-url <ENTER_IMAGE_URL_HERE> 
}

undercloud(){
  infrared tripleo-undercloud --version 16.2 --cdn cdn_creds.yml \
  --ssl false --tls-everywhere false --images-task rpm --images-update no \
  --config-options DEFAULT.undercloud_timezone=UTC
}

overcloud(){
 infrared tripleo-overcloud --version 16.2 --cdn cdn_creds.yml --deployment-files virt --introspect yes --tag yes --containers yes --deploy yes \
 --overcloud-ssl no --overcloud-debug yes --network-ovn yes --network-backend geneve --storage-backend lvm \
 --config-heat Timezone='UTC' --overcloud-templates l3_fip_qos

}

clean(){
 infrared virsh --host-address $HOST --host-key $KEY --cleanup True

}

## GLOBALS
HOST=<ENTER_HOSTNAME_OR_IP_HERE>
KEY=~/.ssh/id_ecdsa

$@
